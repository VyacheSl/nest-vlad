import {HttpException, HttpStatus} from "@nestjs/common";

export class ValidationExeption extends HttpException {
    message

    constructor(res) {
        super(res, HttpStatus.BAD_REQUEST)
        this.message = res
    }

}
