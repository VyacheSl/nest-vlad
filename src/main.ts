import {NestFactory} from "@nestjs/core";
import {AppModule} from "./app.module";
import {DocumentBuilder, SwaggerModule} from "@nestjs/swagger";
import {UseGuards} from "@nestjs/common";
import {JwtAuthGuard} from "./auth/jwt.auth.guard";
import {ValidationPipe} from "./pipes/validation.pipe";


async function start() {
    const PORT = process.env.PORT || 3001;
    const app = await NestFactory.create(AppModule)
    const config = new DocumentBuilder()
        .setTitle('Урок по продвинутому BACKEND')
        .setDescription('Документация REST API')
        .setVersion('0.0.1')
        .addTag('Vyache')
        .build()
    const document = SwaggerModule.createDocument(app, config)
    SwaggerModule.setup('/api/docs', app, document)

    app.useGlobalPipes(new ValidationPipe())

    await app.listen(PORT, () => console.log(`Working on PORT: ${PORT}`))
}

start()
