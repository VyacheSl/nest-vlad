import {Body, Controller, Post} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";
import {CreateUserDto} from "../users/dto/create-user.dto";
import {AuthService} from "./auth.service";

@ApiTags('Авторизация')
@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}
    @Post('/login')
    login(@Body() userDto: CreateUserDto) {
        return this.authService.login(userDto)
    }
    @Post('/registration')
    registration(@Body() userDto: CreateUserDto) {
        return this.authService.registration(userDto)
    }
}
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Imh1b2kiLCJpZCI6OCwicm9sZXMiOlt7ImlkIjoxLCJ2YWx1ZSI6IlVTRVIiLCJkZXNjcmlwdGlvbiI6ItCf0L7Qu9GM0LfQvtCy0LDRgtC10LvRjCIsImNyZWF0ZWRBdCI6IjIwMjItMDktMjVUMjM6MzA6MzAuNjU4WiIsInVwZGF0ZWRBdCI6IjIwMjItMDktMjVUMjM6MzA6MzAuNjU4WiJ9XSwiaWF0IjoxNjY0MTc2MzM4LCJleHAiOjE2NjQyNjI3Mzh9.mRNKAjJZ2FRhRP7UXiLII28EaBXDmBB4_lp_erXOBG0
