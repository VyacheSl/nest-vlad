import {Body, Controller, Get, Param, Post} from '@nestjs/common';
import {RolesService} from "./roles.service";
import {CreateRoleDto} from "./dto/create-role.dto";
import {ApiTags} from "@nestjs/swagger";
@ApiTags('Роли')
@Controller('roles')
export class RolesController {
    constructor(private roleService: RolesService) {
    }

    @Get('')
    getRoles() {
        return this.roleService.getRoles()
    }
    @Get('/:value')
    getByValue(@Param('value') value: string) {
        // return value
        return this.roleService.getRoleByValue(value)
    }

    @Post()
    create(@Body() dto: CreateRoleDto) {
        return this.roleService.createRole(dto)
    }
}
